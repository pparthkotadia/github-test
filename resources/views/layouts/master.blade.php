 <!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"  lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]> <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<!--<![endif]-->
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- meta begins -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />

        {{--CSRF Token--}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- meta ends -->

        <!-- title begins -->
        <title>Github Test</title>
        <link rel="shortcut icon" href="images/favicon.ico" />
        <!-- title ends -->

        <!-- stylesheet begins -->
        {{ Html::style('css/style.css') }}

        @yield('style')
        <!-- stylesheet ends -->

        {{--Head--}}
        @yield('head')
    </head>
    <body class="@yield('body_class')">

        @yield('content')

        {{--Scripts--}}
        {{ Html::script('js/build.js') }}
        <script type="text/javascript"> var base_url = "{{ url('/') }}" </script>
        @yield('script')
    </body>
</html>





