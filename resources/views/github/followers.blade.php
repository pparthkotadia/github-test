@if($followers && count($followers) > 0)
    @foreach($followers as $follower)
        <tr>
            <td><img src="{{ $follower['avatar_url'] }}" width="50px" height="50px"></td>
            <td class="center">{{ $follower['login'] }}</td>
        </tr>
    @endforeach
@endif