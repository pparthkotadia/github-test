<table class="result_table" border="1">
    <tbody>
        <tr>
            <th>GitHub handle</th>
            <td class="center">{{ $user['login'] }}</td>
        </tr>
        @if($followers && count($followers) > 0)
            <tr>
                <th>GitHub followers count</th>
                <td class="center">{{ $user['followers'] }}</td>
            </tr>
            <tr>
                <th colspan="2">GitHub followers</th>
            </tr>
            <tr>
                <td colspan="2" id="followers_list_table">
                    <table border="1" class="table" id="followers_list">
                        <tbody>
                            @include('github.followers')
                        </tbody>
                    </table>
                </td>
            </tr>
        @else
            <tr>
                <th>GitHub followers count</th>
                <td class="center">{{ $user['followers'] }}</td>
            </tr>
        @endif
    </tbody>
</table>
@if($load_more)
    <div class="load_container">
        <button class="btn btn-success" id="load_more_followers" data-total="{{ $user['followers'] }}">Load More</button>
        <img id="loader_img" name="loader_img" src="{{ url('images/loader.gif') }}" />
    </div>
@endif