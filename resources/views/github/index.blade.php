@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="card-header">Github</h2>
                <div class="card">
                    <div class="card-body">
                        <!-- display message/error -->
                        <div class="error-block"></div>
                        <div class="message-block"></div>
                        @include('common.errors')

                        <!-- Git user search block -->
                        <div class="search-block">
                            <form name="git_search" id="git_search">
                                <div class="form-group">
                                    <input type="text" name="user_name" id="user_name" class="form-control" placeholder="Enter username" required />
                                    <input type="submit" id="submit_btn" name="search" value="Search" class="btn btn-success">
                                </div>
                            </form>
                        </div>

                        <!-- Git user search result block -->
                        <div id="search_result">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        var nextPage;

        $(document).ready(function() {
            $( "#git_search" ).validate({
                errorPlacement: function(){
                    return false;
                },
                rules: {
                    user_name: {
                        required: true
                    }
                }
            });

            $( "#git_search" ).submit(function( e ) {
                e.preventDefault();
                nextPage = 1;

                $(".error-block").html('');
                if($( "#git_search" ).valid())
                {
                    $("#submit_btn").prop('disabled', true);
                    $.ajax({
                        type: 'GET',
                        url: "{{ url('/finder') }}",
                        data: $('#git_search').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            if(data.success)
                            {
                                $("#search_result").html(data.view);
                            }
                            else
                            {
                                $(".error-block").html("<p>" + data.msg + "</p>");
                                $("#search_result").html('');
                            }
                            $("#submit_btn").prop('disabled', false);
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            var errorsHtml;

                            errorsHtml = '<div class="alert alert-danger"><ul>';
                            $.each( errors.errors, function( key, value ) {
                                errorsHtml += '<li>' + value + '</li>';
                            });
                            errorsHtml += '</ul></di>';

                            $( '.error-block' ).html( errorsHtml );
                            $("#submit_btn").prop('disabled', false);
                        }
                    });
                }
            });

            $(document).on("click", "#load_more_followers", function( e ) {
                e.preventDefault();
                nextPage = nextPage + 1;

                $("#loader_img").show();
                $("#load_more_followers").hide();
                var user_name = $('#user_name').val();
                var total = $(this).data('total');

                $.ajax({
                    type: 'GET',
                    url: "{{ url('/load_more_followers') }}",
                    data: {'user_name': user_name, page: nextPage, total: total},
                    dataType: 'json',
                    success: function (data) {
                        if(data.success)
                        {
                            $("#loader_img").hide();
                            $("#load_more_followers").show();

                            $("#followers_list tbody").append(data.view);
                            if(! data.load_more)
                            {
                                $("#load_more_followers").remove();
                            }
                        }
                        else
                        {
                            $(".error-block").html("<p>" + data.msg + "</p>");
                        }
                    }
                });
            });
        });
    </script>
@endsection