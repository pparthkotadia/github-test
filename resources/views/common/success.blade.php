<div class="container flash-msg-block">
	@if(Session::has('message'))
		<div class="alert alert-{{ Session::get('message-type') }}">
	        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	        <i class="glyphicon glyphicon-{{ Session::get('message-type') == 'success' ? 'ok' : 'remove'}}"></i> {{ Session::get('message') }}
	    </div>
	@endif
</div>