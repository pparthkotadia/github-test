<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GitRequest;

class GithubController extends Controller
{
    private $client;

    public function __construct(\Github\Client $client)
    {
        $this->client = $client;
        $this->per_page = 30;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('github.index');
    }

    /**
     * Serach git user by username.
     *
     * @return \Illuminate\Http\Response
     */
    public function findGitUser(GitRequest $request)
    {
        $data = $request->all();
        try {
            $user = $this->client->api('user')->show($data['user_name']);

            $page = 1;
            $parameters = [];
            $parameters['per_page'] = $this->per_page;
            $parameters['page'] = $page;

            $followers = $this->client->api('user')->followers($data['user_name'], $parameters);

            $total = $user['followers'];
            $count_page = ceil($total / $this->per_page);
            $load_more = FALSE;
            $load_more = ($count_page > 1) ? TRUE : FALSE;

            $view = \View::make('github.show')->with(['user' => $user, 'followers' => $followers, 'load_more' => $load_more])->render();

            return response()->json(['success' => 1, 'view' => $view]);

        } catch (\RuntimeException $e) {
            //$this->handleAPIException($e);
            return response()->json(['success' => 0, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Load more git followers.
     *
     * @return \Illuminate\Http\Response
     */
    public function loadMoreFollowers(Request $request)
    {
        $data = $request->all();

        try {
            $count_page = ceil($data['total'] / $this->per_page);
            $page = $data['page'];

            $parameters = [];
            $parameters['per_page'] = $this->per_page;
            $parameters['page'] = $page;
            $followers = $this->client->api('user')->followers($data['user_name'], $parameters);

            $load_more = ($page < $count_page) ? TRUE : FALSE;
            $view = \View::make('github.followers')->with(['followers' => $followers])->render();

            return response()->json(['success' => 1, 'view' => $view, 'load_more' => $load_more]);
        } catch (\RuntimeException $e) {
            return response()->json(['success' => 0, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Handle api exception error messages.
     *
     */
    public function handleAPIException($e)
    {
        dd($e->getCode() . ' - ' . $e->getMessage());
    }
}
