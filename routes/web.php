<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Github  */

Route::get('/', ['uses' => 'GithubController@index', 'as' => 'index']);
Route::get('/finder', ['uses' => 'GithubController@findGitUser', 'as' => 'finder']);
Route::get('/load_more_followers', ['uses' => 'GithubController@loadMoreFollowers', 'as' => 'load_more_followers']);
